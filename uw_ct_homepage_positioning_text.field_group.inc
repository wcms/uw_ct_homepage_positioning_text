<?php

/**
 * @file
 * uw_ct_homepage_positioning_text.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_homepage_positioning_text_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload|node|homepage_positioning_text|form';
  $field_group->group_name = 'group_upload';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'homepage_positioning_text';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload an image',
    'weight' => '2',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload an image',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => ' group-upload field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_upload|node|homepage_positioning_text|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Upload an image');

  return $field_groups;
}
