<?php

/**
 * @file
 * uw_ct_homepage_positioning_text.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_homepage_positioning_text_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'homepage_positioning_text_context';
  $context->description = 'Displays positioning text (facts & figures, tagline) on the front page.';
  $context->tag = 'homepage';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'homepage_content_type_addons-homepage-positioning-text' => array(
          'module' => 'homepage_content_type_addons',
          'delta' => 'homepage-positioning-text',
          'region' => 'positioning-text',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Displays positioning text (facts & figures, tagline) on the front page.');
  t('homepage');
  $export['homepage_positioning_text_context'] = $context;

  return $export;
}
