<?php

/**
 * @file
 * uw_ct_homepage_positioning_text.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_homepage_positioning_text_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_homepage_positioning_text_node_info() {
  $items = array(
    'homepage_positioning_text' => array(
      'name' => t('Positioning Text (no longer used)'),
      'base' => 'node_content',
      'description' => t('Can contain anything: for example, interesting facts & figures that relate to at least one of the positioning areas of innovations, connections, impact and global view; text that can speak to Waterloo as a whole or the default feature story on the homepage; a specific achievement or initiative; CTAs. This content type is no longer in use.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
